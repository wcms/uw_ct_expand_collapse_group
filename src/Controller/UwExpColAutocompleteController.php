<?php

namespace Drupal\uw_ct_expand_collapse_group\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a route controller for entity autocomplete form elements.
 */
class UwExpColAutocompleteController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * UwExpColAutocompleteController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Handler for autocomplete request.
   */
  public function handleAutocomplete(Request $request) {

    // At least have some results to return.
    $results = [];

    // Get the typed string from the URL, if it exists.
    if ($input = $request->query->get('q')) {

      // Find out how many exact matches there are.
      $nids = $this->entityTypeManager->getStorage('node')->getQuery()
        ->condition('title', $input)
        ->condition('type', 'uw_ct_expand_collapse_group')
        ->condition('status', NodeInterface::PUBLISHED)
        ->sort('nid', 'ASC')
        ->execute();

      // Step through each of the nids and get the label and
      // the id and setup the results array.
      foreach ($nids as $nid) {

        // Load the node.
        $node = $this->entityTypeManager->getStorage('node')->load($nid);
        $title = $node->label();
        $nid = $node->id();

        $results[] = [
          'value' => $title . ' (' . $nid . ')',
          'label' => $title . ' (' . $nid . ')',
        ];
      }

      // Only look for inexact matches if exact matches exceed the
      // number we would normally show.
      $max_matches = 20;
      $nid_count = count($nids);
      if ($nid_count < $max_matches) {
        $max_matches -= $nid_count;
        // Get the query to return the nids.
        $nids = $this->entityTypeManager->getStorage('node')->getQuery()
          ->condition('title', '%' . $input . '%', 'LIKE')
          ->condition('title', $input, '<>')
          ->condition('type', 'uw_ct_expand_collapse_group')
          ->condition('status', NodeInterface::PUBLISHED)
          ->sort('title', 'ASC')
          ->sort('nid', 'ASC')
          ->range(0, $max_matches)
          ->execute();

        // Step through each of the nids and get the label and
        // the id and setup the results array.
        foreach ($nids as $nid) {

          // Load the node.
          $node = $this->entityTypeManager->getStorage('node')->load($nid);
          $title = $node->label();
          $nid = $node->id();

          $results[] = [
            'value' => $title . ' (' . $nid . ')',
            'label' => $title . ' (' . $nid . ')',
          ];
        }
      }
    }

    // Return the results as a json response, so
    // that entity autocomplete can understand it.
    return new JsonResponse($results);
  }

}
